#include "input_args.h"


InputArgs::InputArgs(int argc, char* argv[])
    : args(argv, argv + argc)
{
    for(auto it = args.begin() + 1; it != args.end(); it++)
    {
        if(it != args.end())
        {
            if(*it == "-f")
            {
                matrixFiles[0] = args.at(std::distance(args.begin(), it + 1));
                matrixFiles[1] = args.at(std::distance(args.begin(), it + 2));
            }
            else if(*it == "-mkl")
            {
                compare_with_mkl = true;
            }
            else if (*it == "-a")
            {
                if (args.at(std::distance(args.begin(), it + 1)) == "omp")
                {
                    api = API::kOpenMP;
                }
                else if (args.at(std::distance(args.begin(), it + 1)) == "tbb")
                {
                    api = API::kTBB;
                }
            }
        }
    }
}