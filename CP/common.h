#pragma once

#include <malloc.h>

#ifdef DEBUG
#define DISABLE_IN_DEBUG(X)
#else
#define DISABLE_IN_DEBUG(X) X
#endif

enum class DataAlign : uint32_t
{
    SSE = 1 << 4,
    AVX = 1 << 5,
    Current = SSE,
};


template<typename T>
constexpr T* MyAlloc(size_t elements_num, DataAlign align)
{
    return static_cast<T*>(_mm_malloc(sizeof(T) * elements_num, static_cast<size_t>(align)));
}

template<typename T>
constexpr void MyDealloc(T* ptr)
{
    return ptr ? _mm_free(ptr) : void();
}