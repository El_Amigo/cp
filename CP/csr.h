#pragma once

#include <memory>
#include <ccomplex>
#include <limits>
#include <omp.h>

#include "common.h"
#include "mmf_matrix.h"
#include "input_args.h"

template<typename T>
class CSR
{
    friend CSR<double>* TestMKL(const MMFMatrix& lhs, const MMFMatrix& rhs);
    friend CSR<double>* TestMySPMM(const MMFMatrix& lhs, const MMFMatrix& rhs, API api);
public:
    CSR(const MMFMatrix& mat) :
        CSR(mat.m_desc, mat.m_rows_num, mat.m_columns_num, mat.m_values_num, mat.m_rows, mat.m_columns, mat.m_values)
    {}
    CSR(MatrixDesc desc, int32_t rows_num, int32_t columns_num, int32_t values_num
        , int32_t rows[], int32_t columns[], std::complex<double> values[]);
    ~CSR();
    CSR(const CSR&) = delete;
    CSR(CSR&&) = delete;
    bool operator==(CSR& rhs);

    static __declspec(restrict) CSR<T>* SpMM_Single(const CSR<T>& lhs, const CSR<T>& rhs);
    static __declspec(restrict) CSR<T>* SpMM_OMP(const CSR<T>& __restrict lhs, const CSR<T>& __restrict rhs);
protected:
    CSR(MatrixDesc desc, int32_t rows_num, int32_t columns_num);
    CSR() {}
protected:
    MatrixDesc m_desc;
    uint32_t* m_pointerB = nullptr;
    uint32_t* m_pointerE = nullptr;
    std::complex<T>* m_values = nullptr;
    uint32_t* m_columns = nullptr;
    uint32_t m_values_num = 0u;
    uint32_t m_rows_num = 0u;
    uint32_t m_columns_num = 0u;
};

template<typename T>
inline CSR<T>::CSR(MatrixDesc desc, int32_t rows_num, int32_t columns_num)
    : m_desc(desc)
    , m_values_num(0)
    , m_rows_num(rows_num)
    , m_columns_num(columns_num)
    , m_pointerB(MyAlloc<uint32_t>(rows_num, DataAlign::Current))
    , m_pointerE(MyAlloc<uint32_t>(rows_num, DataAlign::Current))
{
    memset(m_pointerB, 0, sizeof(uint32_t) * rows_num);
    memset(m_pointerE, 0, sizeof(uint32_t) * rows_num);
}

template<typename T>
inline CSR<T>::CSR(MatrixDesc desc, int32_t rows_num, int32_t columns_num, int32_t values_num, int32_t rows[], int32_t columns[], std::complex<double> values[])
    : m_desc(desc)
    , m_values_num(values_num)
    , m_rows_num(rows_num)
    , m_pointerB(MyAlloc<uint32_t>(rows_num, DataAlign::Current))
    , m_columns_num(columns_num)
    , m_pointerE(MyAlloc<uint32_t>(rows_num, DataAlign::Current))
    , m_values(MyAlloc<std::complex<T>>(values_num, DataAlign::Current))
    , m_columns(MyAlloc<uint32_t>(values_num, DataAlign::Current))
{
    memset(m_pointerB, 0, sizeof(uint32_t) * rows_num);
    memset(m_pointerE, 0, sizeof(uint32_t) * rows_num);

    for (int32_t i = 0; i < values_num; i++)
    {
        m_pointerE[rows[i] - m_desc.m_indexBase]++;
    }

    for (int32_t i = 0; i < rows_num - 1; i++)
    {
        m_pointerE[i + 1] += m_pointerE[i];
    }

    for (int32_t i = 0; i < rows_num - 1; i++)
    {
        m_pointerB[i + 1] = m_pointerE[i];
    }

    for (int32_t i = 0; i < values_num; i++)
    {
        int32_t row_ind = rows[i] - m_desc.m_indexBase;

        m_values[m_pointerB[row_ind]] = values[i];
        m_columns[m_pointerB[row_ind]] = columns[i] - m_desc.m_indexBase;

        m_pointerB[row_ind]++;
    }

    m_pointerB[0] = 0u;

    for (uint32_t i = 1; i < m_rows_num; i++)
    {
        m_pointerB[i] = m_pointerE[i - 1];
    }

    m_desc.m_indexBase = static_cast<uint32_t>(SparseIndexBase::kZero);
}

template<typename T>
inline CSR<T>::~CSR()
{
    MyDealloc(m_pointerB);
    MyDealloc(m_pointerE);
    MyDealloc(m_values);
    MyDealloc(m_columns);
}

template<typename T>
inline bool CSR<T>::operator==(CSR & rhs)
{
    if ( m_desc != rhs.m_desc
        || m_columns_num != rhs.m_columns_num
        || m_values_num != rhs.m_values_num
        || m_rows_num != rhs.m_rows_num)
    {
        return false;
    }

    for (uint32_t i = 0; i < rhs.m_rows_num; i++)
    {
        if (m_pointerB[i] != rhs.m_pointerB[i]
            || m_pointerE[i] != rhs.m_pointerE[i])
        {
            return false;
        }
    }

    for (uint32_t i = 0; i < rhs.m_values_num; i++)
    {
        if (m_columns[i] != rhs.m_columns[i]
            || std::abs(m_values[i] - rhs.m_values[i]) > std::numeric_limits<T>::epsilon())
        {
            return false;
        }
    }

    return true;
}

template<typename T>
inline CSR<T>* CSR<T>::SpMM_Single(const CSR<T>& __restrict lhs, const CSR<T>& __restrict rhs)
{

    MatrixDesc desc = rhs.m_desc;
    desc.m_dataType = DataType::kComplex;
    desc.m_indexBase = static_cast<uint32_t>(SparseIndexBase::kZero);
    desc.m_storageScheme = StorageScheme::kGeneral;


    CSR<T>* result = new CSR<T>(desc, lhs.m_rows_num, rhs.m_columns_num);
    //Symbolic multiplication to know the final size of the matrix
    {
        bool* symbolic_matrix_row = MyAlloc<bool>(result->m_columns_num, DataAlign::Current);
        for (uint32_t row_ind = 0; row_ind < result->m_rows_num; row_ind++)
        {
            memset(symbolic_matrix_row, 0, sizeof(bool) * result->m_columns_num);
            #ifndef DEBUG
            #pragma vector aligned
            #endif
            for (uint32_t lhs_val_ind = lhs.m_pointerB[row_ind]; lhs_val_ind < lhs.m_pointerE[row_ind]; lhs_val_ind++)
            {
                #ifndef DEBUG
                __assume_aligned(symbolic_matrix_row, DataAlign::Current);
                #pragma vector aligned
                #endif
                for (uint32_t rhs_val_ind = rhs.m_pointerB[lhs.m_columns[lhs_val_ind]]; rhs_val_ind < rhs.m_pointerE[lhs.m_columns[lhs_val_ind]]; rhs_val_ind++)
                {
                    symbolic_matrix_row[rhs.m_columns[rhs_val_ind]] = true;
                }
            }
            #ifndef DEBUG
            __assume_aligned(symbolic_matrix_row, DataAlign::Current);
            #endif
            for (uint32_t col_ind = 0; col_ind < result->m_columns_num; col_ind++)
            {
                result->m_values_num += static_cast<uint32_t>(symbolic_matrix_row[col_ind]);
            }

            result->m_pointerE[row_ind] = result->m_values_num;
        }

        MyDealloc(symbolic_matrix_row);
    }
    //Symbolic multiplication to know the final size of the matrix

    result->m_pointerB[0] = 0;
    for (uint32_t i = 0; i < result->m_rows_num - 1; i++)
    {
        result->m_pointerB[i + 1] = result->m_pointerE[i];
    }

    result->m_columns = MyAlloc<uint32_t>(result->m_values_num, DataAlign::Current);
    result->m_values = MyAlloc<std::complex<T>>(result->m_values_num, DataAlign::Current);

    {
        bool* symbolic_matrix_row = MyAlloc<bool>(result->m_columns_num, DataAlign::Current);
        std::complex<T>* values_row = MyAlloc<std::complex<T>>(result->m_columns_num, DataAlign::Current);

        for (uint32_t row_ind = 0; row_ind < result->m_rows_num; row_ind++)
        {
            memset(symbolic_matrix_row, 0, sizeof(bool) * result->m_columns_num);
            memset(values_row, 0, sizeof(std::complex<T>) * result->m_columns_num);

            #ifndef DEBUG
            #pragma vector aligned
            #endif
            for (uint32_t lhs_val_ind = lhs.m_pointerB[row_ind]; lhs_val_ind < lhs.m_pointerE[row_ind]; lhs_val_ind++)
            {
                #ifndef DEBUG
                #pragma vector aligned
                #endif
                for (uint32_t rhs_val_ind = rhs.m_pointerB[lhs.m_columns[lhs_val_ind]]; rhs_val_ind < rhs.m_pointerE[lhs.m_columns[lhs_val_ind]]; rhs_val_ind++)
                {
                    symbolic_matrix_row[rhs.m_columns[rhs_val_ind]] = true;
                    values_row[rhs.m_columns[rhs_val_ind]] = values_row[rhs.m_columns[rhs_val_ind]] + lhs.m_values[lhs_val_ind] * rhs.m_values[rhs_val_ind];
                }
            }

            for (uint32_t col_ind = 0; col_ind < result->m_columns_num; col_ind++)
            {
                if (symbolic_matrix_row[col_ind])
                {
                    result->m_columns[result->m_pointerB[row_ind]] = col_ind;
                    result->m_values[result->m_pointerB[row_ind]]  = values_row[col_ind];
                    result->m_pointerB[row_ind]++;
                }
            }
        }

        MyDealloc(symbolic_matrix_row);
        MyDealloc(values_row);
    }

    result->m_pointerB[0] = 0;
    for (uint32_t i = 0; i < result->m_rows_num - 1; i++)
    {
        result->m_pointerB[i + 1] = result->m_pointerE[i];
    }


    return result;
}

template<typename T>
inline CSR<T>* CSR<T>::SpMM_OMP(const CSR<T>& __restrict lhs, const CSR<T>& __restrict rhs)
{

    MatrixDesc desc = rhs.m_desc;
    desc.m_dataType = DataType::kComplex;
    desc.m_indexBase = static_cast<uint32_t>(SparseIndexBase::kZero);
    desc.m_storageScheme = StorageScheme::kGeneral;


    CSR<T>* result = new CSR<T>(desc, lhs.m_rows_num, rhs.m_columns_num);
    //Symbolic multiplication to know the final size of the matrix
    #pragma omp parallel shared(result, rhs, lhs) //num_threads(4)
    {
        __declspec(align(static_cast<size_t>(DataAlign::Current))) bool* symbolic_matrix_row = MyAlloc<bool>(result->m_columns_num, DataAlign::Current);

        //#pragma omp parallel for shared(result, rhs, lhs)
        int32_t  numOfThreads = omp_get_num_threads();
        int32_t  threadNum    = omp_get_thread_num();
        uint32_t chunk        = result->m_rows_num / numOfThreads;
        uint32_t chunkResidue = result->m_rows_num % chunk;
        uint32_t chunkStart   = chunk * threadNum;
        uint32_t chunkEnd     = chunkStart + chunk + (numOfThreads - 1 == threadNum) * chunkResidue;
        for (uint32_t row_ind = chunkStart; row_ind < chunkEnd; row_ind++)
        {
            memset(symbolic_matrix_row, 0, sizeof(bool) * result->m_columns_num);
            const uint32_t rowStart = lhs.m_pointerB[row_ind];
            const uint32_t rowEnd   = lhs.m_pointerE[row_ind];
            #pragma vector aligned
            for (uint32_t lhs_val_ind = rowStart; lhs_val_ind < rowEnd; lhs_val_ind++)
            {
                __assume_aligned(symbolic_matrix_row, DataAlign::Current);
                #pragma vector aligned
                for (uint32_t rhs_val_ind = rhs.m_pointerB[lhs.m_columns[lhs_val_ind]]; rhs_val_ind < rhs.m_pointerE[lhs.m_columns[lhs_val_ind]]; rhs_val_ind++)
                {
                    symbolic_matrix_row[rhs.m_columns[rhs_val_ind]] = true;
                }
            }
            //__assume_aligned(symbolic_matrix_row, DataAlign::Current);
            //#pragma simd reduction(+:values_num)
            for (uint32_t col_ind = 0; col_ind < result->m_columns_num; col_ind++)
            {
                result->m_pointerE[row_ind] += static_cast<uint32_t>(symbolic_matrix_row[col_ind]);
            }
        }

        MyDealloc(symbolic_matrix_row);
    }
    //Symbolic multiplication to know the final size of the matrix

    for (uint32_t i = 0; i < result->m_rows_num - 1; i++)
    {
        result->m_pointerE[i + 1] += result->m_pointerE[i];
    }

    result->m_values_num = result->m_pointerE[result->m_rows_num - 1];
    result->m_pointerB[0] = 0;

    const uint32_t rows_num = result->m_rows_num;
    for (uint32_t i = 0; i < rows_num - 1; i++)
    {
        result->m_pointerB[i + 1] = result->m_pointerE[i];
    }

    result->m_columns = MyAlloc<uint32_t>(result->m_values_num, DataAlign::Current);
    result->m_values = MyAlloc<std::complex<T>>(result->m_values_num, DataAlign::Current);

    #pragma omp parallel shared(result, rhs, lhs)
    {
        std::complex<T>* values_row = MyAlloc<std::complex<T>>(result->m_columns_num, DataAlign::Current);

        //#pragma omp parallel for shared(result, rhs, lhs)
        int32_t  numOfThreads = omp_get_num_threads();
        int32_t  threadNum = omp_get_thread_num();
        uint32_t chunk = result->m_rows_num / numOfThreads;
        uint32_t chunkResidue = result->m_rows_num % chunk;
        uint32_t chunkStart = chunk * threadNum;
        uint32_t chunkEnd = chunkStart + chunk + (numOfThreads - 1 == threadNum) * chunkResidue;
        for (uint32_t row_ind = chunkStart; row_ind < chunkEnd; row_ind++)
        {
            memset(values_row, 0, sizeof(std::complex<T>) * result->m_columns_num);

            //#pragma vector aligned
            const uint32_t rowStart = lhs.m_pointerB[row_ind];
            const uint32_t rowEnd = lhs.m_pointerE[row_ind];
            for (uint32_t lhs_val_ind = rowStart; lhs_val_ind < rowEnd; lhs_val_ind++)
            {
                //#pragma vector aligned
                const uint32_t rhsRowStart = rhs.m_pointerB[lhs.m_columns[lhs_val_ind]];
                const uint32_t rhsRowEnd = rhs.m_pointerE[lhs.m_columns[lhs_val_ind]];
                for (uint32_t rhs_val_ind = rhsRowStart; rhs_val_ind < rhsRowEnd; rhs_val_ind++)
                {
                    uint32_t column = rhs.m_columns[rhs_val_ind];
                    std::complex<T> lres = lhs.m_values[lhs_val_ind];
                    std::complex<T> rres = rhs.m_values[rhs_val_ind];
                    std::complex<T> res = lres * rres;
                    values_row[column] += res;//lhs.m_values[lhs_val_ind] * rhs.m_values[rhs_val_ind];
                }
            }

            for (uint32_t col_ind = 0; col_ind < result->m_columns_num; col_ind++)
            {
                if (std::abs(values_row[col_ind]) > std::numeric_limits<T>::epsilon())
                {
                    result->m_columns[result->m_pointerB[row_ind]] = col_ind;
                    result->m_values[result->m_pointerB[row_ind]] = values_row[col_ind];
                    result->m_pointerB[row_ind]++;
                }
            }
        }

        MyDealloc(values_row);
    }

    result->m_pointerB[0] = 0;
    for (uint32_t i = 0; i < result->m_rows_num - 1; i++)
    {
        result->m_pointerB[i + 1] = result->m_pointerE[i];
    }


    return result;
}