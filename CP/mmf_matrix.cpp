#include <algorithm>

#include "mmf_matrix.h"
#include "common.h"

extern "C"
{
    #include "mmio.h"
}

MMFMatrix::MMFMatrix(const char* path)
    : m_desc(MatrixFormat::kNone, DataType::kNone, StorageScheme::kNone, SparseIndexBase::kOne)
{
    FILE* matrix_file = nullptr;
    fopen_s(&matrix_file, path, "r");
    if(!matrix_file)
    {
        throw std::invalid_argument((std::string("Could not open matrix market file from path ") + (path ? path : " nullptr")).c_str());
    }

    MM_typecode matrix_typecode;

    if (mm_read_banner(matrix_file, &matrix_typecode))
    {
        throw std::runtime_error("Could not read the Matrix Market banner.");
    }

    m_desc = MatrixDesc(matrix_typecode, SparseIndexBase::kOne);

    if (m_desc.m_dataType != DataType::kComplex)
    {
        throw std::exception("Complex matrices only!");
    }

    if (mm_read_mtx_crd_size(matrix_file, &m_rows_num, &m_columns_num, &m_values_num))
    {
        throw std::runtime_error("Could not read the matrix size.");
    }

    m_rows    = MyAlloc<int32_t>(m_values_num, DataAlign::Current);
    m_columns = MyAlloc<int32_t>(m_values_num, DataAlign::Current);
    m_values  = MyAlloc<std::complex<double>>(m_values_num, DataAlign::Current);

    int32_t failed = mm_read_mtx_crd_data(matrix_file, m_rows_num, m_columns_num, m_values_num, m_rows, m_columns, reinterpret_cast<double*>(m_values), matrix_typecode);

    fclose(matrix_file);

    if (failed)
    {
        throw std::runtime_error("Failed to read matrix data.");
    }

    if(mm_is_pattern(matrix_typecode))
    {
        double* const m_double_values = reinterpret_cast<double*>(m_values);
        const int32_t local_values_num = m_values_num;
        for (int32_t i = 0; i < local_values_num; i++)
        {
            m_double_values[i] = 1.0;
        }
    }
}

MMFMatrix::~MMFMatrix()
{
    MyDealloc(m_rows);
    MyDealloc(m_columns);
    MyDealloc(m_values);
}
