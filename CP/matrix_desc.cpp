#include "matrix_desc.h"
extern "C"
{
    #include "mmio.h"
}

/******************** Matrix Market internal definitions ********************

MM_matrix_typecode: 4-character sequence

ojbect      sparse/     data        storage
dense       type        scheme

string position:     [0]        [1]     [2]         [3]

Matrix typecode:  M(atrix)    C(oord)    R(eal)    G(eneral)
                              A(array)   C(omplex) H(ermitian)
                                         P(attern) S(ymmetric)
                                         I(nteger) K(kew)

***********************************************************************/

MatrixDesc::MatrixDesc()
{}

bool MatrixDesc::operator==(MatrixDesc& rhs)
{
    return (m_dataType == rhs.m_dataType
        && m_indexBase == rhs.m_indexBase
        && m_matrixFormat == rhs.m_matrixFormat
        && m_storageScheme == rhs.m_storageScheme);
}

bool MatrixDesc::operator!=(MatrixDesc& rhs)
{
    return !operator==(rhs);
}

MatrixDesc::MatrixDesc(const MM_typecode& mmfTypecode, SparseIndexBase indexBase)
    : m_indexBase(static_cast<uint32_t>(indexBase))
{
    switch(mmfTypecode[1])
    {
        case 'C': m_matrixFormat = MatrixFormat::kSparseCoordinates;
            break;
        case 'A': m_matrixFormat = MatrixFormat::kDenseArray;
            break;
    }
    switch(mmfTypecode[2])
    {
        case 'R': m_dataType = DataType::kReal;
            break;
        case 'C':  m_dataType = DataType::kComplex;
            break;
        case 'P':  m_dataType = DataType::kPattern;
            break;
        case 'I':  m_dataType = DataType::kInteger;
            break;
    }
    switch(mmfTypecode[3])
    {
        case 'G': m_storageScheme = StorageScheme::kGeneral;
            break;
        case 'H': m_storageScheme = StorageScheme::kHermitian;
            break;
        case 'K': m_storageScheme = StorageScheme::kSkewSymmetric;
            break;
        case 'S': m_storageScheme = StorageScheme::kSymmetric;
            break;
    }
}

MatrixDesc::MatrixDesc(MatrixFormat matrixFormat, DataType dataType, StorageScheme storageScheme, SparseIndexBase indexBase)
    : m_matrixFormat(matrixFormat)
    , m_dataType(dataType)
    , m_storageScheme(storageScheme)
    , m_indexBase(static_cast<uint32_t>(indexBase))
{}
