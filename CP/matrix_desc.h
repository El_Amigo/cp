#pragma once

#include <cstdint>

/******************** Matrix Market internal definitions ********************

MM_matrix_typecode: 4-character sequence

ojbect      sparse/     data        storage
dense       type        scheme

string position:     [0]        [1]     [2]         [3]

Matrix typecode:  M(atrix)    C(oord)    R(eal)    G(eneral)
                              A(array)   C(omplex) H(ermitian)
                                         P(attern) S(ymmetric)
                                         I(nteger) K(kew)

***********************************************************************/

enum class StorageFormat { kNone, kDense, kCoordinate, kCSR, kCSR3, kCSC, kCSC3, kSkyline, kBSR, kBSR3, kDiagonal };

enum class MatrixFormat { kNone, kSparseCoordinates, kDenseArray };
enum class DataType { kNone, kReal, kComplex, kPattern, kInteger };
enum class StorageScheme { kNone, kGeneral, kHermitian, kSymmetric, kSkewSymmetric };

enum class SparseIndexBase : uint32_t
{
    kZero = 0,           /* C-style */
    kOne = 1            /* Fortran-style */
};

typedef char MM_typecode[4];

struct MatrixDesc
{
    MatrixDesc();
    MatrixDesc(const MM_typecode& mmf_typecode, SparseIndexBase m_indexBase);
    MatrixDesc(MatrixFormat matrixFormat, DataType dataType, StorageScheme storageScheme, SparseIndexBase indexBase);
    bool operator==(MatrixDesc& rhs);
    bool operator!=(MatrixDesc& rhs);
    MatrixFormat m_matrixFormat = MatrixFormat::kNone;
    DataType m_dataType = DataType::kNone;
    StorageScheme m_storageScheme = StorageScheme::kNone;
    uint32_t m_indexBase = 0u;
};