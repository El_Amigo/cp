#pragma once
#include <vector>
#include <map>

enum class API : size_t
{
    kSingleThread = static_cast<size_t>(0),
    kOpenMP = static_cast<size_t>(1),
    kTBB = static_cast<size_t>(2),
    kMPI = static_cast<size_t>(3),
};

class InputArgs
{
public:
    InputArgs(int argc, char* argv[]);
public:
    API  api = API::kSingleThread;
    bool compare_with_mkl = false;
    std::string matrixFiles[2];
private:
    std::vector<std::string> args;
};