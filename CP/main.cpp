#include <string>
#include <iostream>
#include <cstdint>
#include <memory>

#include "test.h"
#include "input_args.h"

int main(int argc, char* argv[])
{
    try 
    {

        InputArgs args(argc, argv);

        MMFMatrix lhs(args.matrixFiles[0]);
        MMFMatrix rhs(args.matrixFiles[1]);

        std::unique_ptr<CSR<double>> my_result(TestMySPMM(lhs, rhs, args.api));

        if (args.compare_with_mkl)
        {
            std::unique_ptr<CSR<double>> mkl_result(TestMKL(lhs, rhs));

            if (*my_result == *mkl_result)
            {
                std::cout << "Success: MKL result == MySPMM result" << std::endl;
            }
            else
            {
                std::cout << "Failed: MKL result != MySPMM result" << std::endl;
            }
        }
    }
    catch (std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch (...) {
        std::cerr << "Exception of unknown type!\n";
    }

    return 0;
}