#pragma once
#include <complex>
#include <vector>
#include <chrono>
#include <algorithm>
#include <numeric>
#define MKL_Complex8 std::complex<float>
#define MKL_Complex16 std::complex<double>
#include <mkl_types.h>
#include <mkl.h>

#include "mmf_matrix.h"
#include "csr.h"

#define TIMED_FUNC_NRV(func)                                                                  \
    {double duration;                                                                      \
    std::chrono::high_resolution_clock::time_point begin, end;                            \
    begin = std::chrono::high_resolution_clock::now();                                    \
    func;                                                                \
    end = std::chrono::high_resolution_clock::now();                                      \
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();\
    std::cout << "MKL: " << duration << std::endl;}


#define TIMED_FUNC(ret, func)                                                                  \
    {double duration;                                                                      \
    std::chrono::high_resolution_clock::time_point begin, end;                            \
    begin = std::chrono::high_resolution_clock::now();                                    \
    ret = std::move(func);                                                                \
    end = std::chrono::high_resolution_clock::now();                                      \
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();\
    std::cout << "MySPMM: " << duration << std::endl;}

sparse_matrix_t CreateMKLCSR(const MMFMatrix& mtx)
{
    sparse_matrix_t mkl_mtx = nullptr;

    mkl_sparse_z_create_coo(&mkl_mtx, (sparse_index_base_t)MKL_INT(1)
        , mtx.m_rows_num, mtx.m_columns_num
        , mtx.m_values_num, mtx.m_rows
        , mtx.m_columns, mtx.m_values);

    sparse_status_t status = SPARSE_STATUS_SUCCESS;
    status = mkl_sparse_convert_csr(mkl_mtx, SPARSE_OPERATION_NON_TRANSPOSE, &mkl_mtx);

    if (status != SPARSE_STATUS_SUCCESS)
    {
        throw std::runtime_error("MKL matrix conversion to CSR failed\n");
    }

    mkl_sparse_optimize(mkl_mtx);

    return mkl_mtx;
}

template<typename T, typename K>
inline void ReallocMKLArray(T** csr_dst, K* mkl_source, size_t elem_num)
{
    *csr_dst  = MyAlloc<T>(elem_num, DataAlign::Current);
    memcpy_s(*csr_dst, sizeof(T) * elem_num, mkl_source, sizeof(K) * elem_num);
}

CSR<double>* TestMKL(const MMFMatrix& lhs, const MMFMatrix& rhs)
{
    sparse_matrix_t mkl_lhs = CreateMKLCSR(lhs);
    sparse_matrix_t mkl_rhs = CreateMKLCSR(rhs);
    sparse_matrix_t mkl_result  = nullptr;

    TIMED_FUNC_NRV(mkl_sparse_spmm(SPARSE_OPERATION_NON_TRANSPOSE, mkl_lhs, mkl_rhs, &mkl_result));

    CSR<double>* result = new CSR<double>();
    sparse_index_base_t indexing;

    MKL_INT* mkl_columns = nullptr;
    MKL_INT* mkl_pointerB = nullptr;
    MKL_INT* mkl_pointerE = nullptr;
    std::complex<double>* mkl_values = nullptr;

    mkl_sparse_z_export_csr(mkl_result, &indexing, reinterpret_cast<MKL_INT*>(&result->m_rows_num), reinterpret_cast<MKL_INT*>(&result->m_columns_num)
        , &mkl_pointerB, &mkl_pointerE, &mkl_columns, &mkl_values);

    result->m_values_num = mkl_pointerE[result->m_rows_num - 1];

//Reallocing and sorting MKL result for comparison
    ReallocMKLArray(&result->m_columns, mkl_columns, result->m_values_num);
    ReallocMKLArray(&result->m_values, mkl_values, result->m_values_num);
    ReallocMKLArray(&result->m_pointerB, mkl_pointerB, result->m_rows_num);
    ReallocMKLArray(&result->m_pointerE, mkl_pointerE, result->m_rows_num);

    for (uint32_t i = 0; i < result->m_rows_num; i++)
    {
        std::vector<uint32_t> sorted_indexes(result->m_pointerE[i] - result->m_pointerB[i], 0);
        std::iota(sorted_indexes.begin(), sorted_indexes.end(), 0u);
        std::sort(sorted_indexes.begin(), sorted_indexes.end(),
        [&](const uint32_t& a, const uint32_t& b) 
        {
            return (result->m_columns[result->m_pointerB[i] + a] < result->m_columns[result->m_pointerB[i] + b]);
        });

        std::vector<uint32_t> temp_ind(sorted_indexes.size());
        std::vector<std::complex<double>> temp_val(sorted_indexes.size());

        for (uint32_t s = 0; s < sorted_indexes.size(); s++)
        {
            temp_ind[s] = result->m_columns[result->m_pointerB[i] + sorted_indexes[s]];
            temp_val[s] = result->m_values[result->m_pointerB[i] + sorted_indexes[s]];
        }

        for (uint32_t s = 0; s < sorted_indexes.size(); s++)
        {
            result->m_columns[result->m_pointerB[i] + s] = temp_ind[s];
            result->m_values[result->m_pointerB[i] + s] = temp_val[s];
        }
    }
//Reallocing and sorting MKL result for comparison

    result->m_desc = { MatrixFormat::kSparseCoordinates, DataType::kComplex, StorageScheme::kGeneral, static_cast<SparseIndexBase>(indexing) };

    mkl_sparse_destroy(mkl_lhs);
    mkl_sparse_destroy(mkl_rhs);
    mkl_sparse_destroy(mkl_result);
    mkl_free_buffers();

    return result;
}

#undef MKL_Complex8
#undef MKL_Complex16

CSR<double>* TestMySPMM(const MMFMatrix& lhs, const MMFMatrix& rhs, API api)
{
    CSR<double> my_lhs(lhs);
    CSR<double> my_rhs(rhs);

    CSR<double>* result = nullptr;

    switch (api)
    {
    case API::kSingleThread:
        TIMED_FUNC(result, CSR<double>::SpMM_Single(my_lhs, my_rhs));
    break;
    case API::kOpenMP:
        omp_set_num_threads(omp_get_max_threads());
        TIMED_FUNC(result, CSR<double>::SpMM_OMP(my_lhs, my_rhs));
    break;
    default:
        throw std::logic_error("Unknown API!");
    break;
    }
    return std::move(result);
}