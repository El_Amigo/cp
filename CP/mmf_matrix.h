#pragma once
#include <string>
#include <complex>

#include "matrix_desc.h"

class MMFMatrix
{
public:
    MMFMatrix(const std::string& path) : MMFMatrix(path.c_str()) {}
    MMFMatrix(const char* path);
    ~MMFMatrix();
private:
    MMFMatrix();
public:
    MatrixDesc m_desc;
    int32_t m_rows_num = 0;
    int32_t m_columns_num = 0;
    int32_t m_values_num = 0;
    int32_t* m_rows = nullptr;
    int32_t* m_columns = nullptr;
    std::complex<double>* m_values = nullptr;
};